
filename=hosts.txt
declare -a hosttoping
hosttoping=(`cat "$filename"`)

for i in ${!hosttoping[@]}
  do
  echo "$i"
  rrdtool create latency_db_$i.rrd --step 180 DS:pl:GAUGE:540:0:100 DS:rtt:GAUGE:540:0:10000000 RRA:MAX:0.5:1:500
done;
