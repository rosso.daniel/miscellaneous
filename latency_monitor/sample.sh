#!/bin/bash

filename=hosts.txt
declare -a hosttoping
hosttoping=(`cat "$filename"`)



### set the paths
command="/bin/ping -q -n -c 3"
gawk="/usr/bin/gawk"
rrdtool="/usr/bin/rrdtool"

### data collection routine
get_data() {
    local output=$($command $1 2>&1)
    local method=$(echo "$output" | $gawk '
        BEGIN {pl=100; rtt=0.1}
        /packets transmitted/ {
            match($0, /([0-9]+)% packet loss/, datapl)
            pl=datapl[1]
        }
        /min\/avg\/max/ {
            match($4, /(.*)\/(.*)\/(.*)\/(.*)/, datartt)
            rtt=datartt[2]
        }
        END {print pl ":" rtt}
        ')
    RETURN_DATA=$method
}


for i in ${!hosttoping[@]}
  do
  echo "$i"
  echo ${hosttoping[$i]}
  get_data ${hosttoping[$i]}
  echo $RETURN_DATA
  echo rrdtool update latency_db_$i.rrd --template pl:rtt N:$RETURN_DATA
done;

#hoststoping
### collect the data
#get_data $hosttoping

#echo $RETURN_DATA
### update the database
#$rrdtool update latency_db.rrd --template pl:rtt N:$RETURN_DATA
