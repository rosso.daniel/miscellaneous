#!/bin/bash
filename=../hosts.txt
declare -a hosttoping
hosttoping=(`cat "$filename"`)
rm *.png
rm *.rrd
for i in ${!hosttoping[@]}
  do
  echo "$i"
  scp i-tera@192.168.193.1:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/1_facu_$i.rrd
  scp i-tera@192.168.193.2:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/2_lucas_$i.rrd
  scp i-tera@192.168.193.3:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/3_pelado_$i.rrd
  scp i-tera@192.168.193.4:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/4_mati_$i.rrd
  scp i-tera@192.168.193.5:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/5_ivan_$i.rrd
  scp i-tera@192.168.193.6:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/6_gaby_$i.rrd
  scp i-tera@192.168.193.7:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/7_fede_$i.rrd
  scp i-tera@192.168.193.8:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/8_fer_$i.rrd
  scp i-tera@192.168.193.9:/home/i-tera/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/9_agus_$i.rrd

  scp drosso@192.168.193.254:/home/drosso/latency_monitor/latency_db_$i.rrd /home/i-tera/Daniel/latency_monitor/forti/collected_rrd/254_homero_$i.rrd

done;
