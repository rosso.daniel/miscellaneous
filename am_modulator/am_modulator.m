
clc;
clear all;
close all;



%%%SIMULATION PARAMETERS
Fs = 100000; %100K       
t=[0:1/Fs:(4096-1)/Fs];




%%CARRIER DEFINITION
c_amp=2;
c_frec=(Fs/256)*35;
carrier=c_amp*cos(2*pi*c_frec*t);
carrier_fft = abs((fftshift(fft(carrier))));
carrier_freq_vector = linspace(-Fs/2,Fs/2,length(carrier_fft));


%%MODULATING DEFINITION
m_amp=0.5;
m_frec=(Fs/256)*2;
modulating=m_amp*cos(2*pi*m_frec*t);
modulating_fft = abs((fftshift(fft(modulating))));
modulating_freq_vector = linspace(-2048*Fs/4096,2047*Fs/4096,length(modulating_fft));

%%MODULATION INDEX
m=1;


%%AM MODULATION
am_ps=carrier.*modulating*m;
am=am_ps+carrier;
am_fft = abs((fftshift(fft(am))));
am_freq_vector = linspace(-Fs/2,Fs/2,length(am_fft));
am_freq_vector = linspace(-Fs/2,Fs/2,length(am_fft));

%%NOISE GENERATION
n_amp=0.5;
n_frec=100;
noise=1+n_amp*cos(2*pi*n_frec*t);
noise_fft = abs((fftshift(fft(noise))));
noise_freq_vector = linspace(-Fs/2,Fs/2,length(noise));

target_power=am.^2;
target_power=target_power./1;


am_noised=am;%.*noise;

am_noised_fft = abs((fftshift(fft(am_noised))));
am_noised_vector = linspace(-Fs/2,Fs/2,length(am_noised));



kp=0.01;
ki=0.001;
error_estimator_sr=[];
error_estimator=[];
acum=0;
compensator=1;
loop=0;
batch_samples=256;

am_peaks_final=[];
r_flt_final=[];
final_final=[];

filter_state=[];
for i=1:((length(am_noised)/batch_samples))
    loop=i-1;
    am_noised(((batch_samples*loop)+1):(batch_samples*(loop+1))).*compensator;

    %error_estimator_sr     = [am_noised(((batch_samples*loop)+1):(batch_samples*(loop+1)))];
    %error_estimator(i)=var(error_estimator_sr);
   %acum=acum+error_estimator(i);
   %compensator=error_estimator(i)*kp+acum*ki;  





%%AM ENVELOPE DETECTION
am_peaks_final(((batch_samples*loop)+1):(batch_samples*(loop+1)))=abs(am_noised(((batch_samples*loop)+1):(batch_samples*(loop+1))));

%disp(((batch_samples*loop)+1))
%disp((batch_samples*(loop+1)))

%%AM RECOVERY
[b,a] = butter(1,2*pi*m_frec/Fs);
[r_flt_final(((batch_samples*loop)+1):(batch_samples*(loop+1))),filter_state] = filter(b,a,am_peaks_final(((batch_samples*loop)+1):(batch_samples*(loop+1))),filter_state);
final_final(((batch_samples*loop)+1):(batch_samples*(loop+1))) = r_flt_final(((batch_samples*loop)+1):(batch_samples*(loop+1))) - mean(r_flt_final(((batch_samples*loop)+1):(batch_samples*(loop+1))) );


%r_flt = lowpass(am_peaks,2*m_frec*pi/(1/fs)); %%filter(b,a,am_peaks);

%final = r_flt-mean(r_flt);
%final_fft = abs((fftshift(fft(final))));
%final_fft_vector = linspace(-Fs/2,Fs/2,length(final_fft));


end
am_peaks_fft = abs((fftshift(fft(am_peaks_final))));
am_peaks_fft_vector = linspace(-Fs/2,Fs/2,length(am_peaks_fft));
final_fft = abs((fftshift(fft(final_final))));
final_fft_vector = linspace(-2048*Fs/4096,2047*Fs/4096,length(final_fft));


%%%%%FREQUENCY ANALYSIS GRAPHICS
figure;
subplot(7,1,1); 
plot(modulating_freq_vector,modulating_fft); xlabel('Frequency'); ylabel('Modulating'); xlim([-m_frec*1.2 m_frec*1.2]);
subplot(7,1,2); 
plot(carrier_freq_vector,carrier_fft); xlabel('Frequency'); ylabel('Carrier'); xlim([-c_frec*1.2 c_frec*1.2]);
subplot(7,1,3); 
plot(am_freq_vector,am_fft); xlabel('Frequency'); ylabel('AM signal'); xlim([-c_frec*1.2 c_frec*1.2]);
subplot(7,1,4); 
plot(am_noised_vector,am_noised_fft); xlabel('Frequency'); ylabel('AM signal + Noise'); xlim([-c_frec*1.2 c_frec*1.2]);
%subplot(7,1,5); 
%plot(am_feedback_filtered_fft_vector,am_feedback_filtered_fft); xlabel('Frequency'); ylabel('AM signal - Noise'); xlim([-c_frec*1.2 c_frec*1.2]);
subplot(7,1,6);
plot(am_peaks_fft_vector,am_peaks_fft); xlabel('Frequency'); ylabel('Am peaks'); xlim([-m_frec*1.2 m_frec*1.2]);
subplot(7,1,7);
plot(final_fft_vector,final_fft); xlabel('Frequency'); ylabel('Modulating'); xlim([-m_frec*1.2 m_frec*1.2]);

%%%

%%%%%TIME ANALYSIS GRAPHICS
figure
subplot(9,1,1);%plotting the message signal wave
plot(t,modulating);
ylabel('Modulating signal');

subplot(9,1,2); %plotting the carrier signal wave
plot(t,carrier);
ylabel('Carrier');

subplot(9,1,3); %plotting the carrier signal wave
plot(t,noise);
ylabel('Noise');


subplot(9,1,4); %plotting the amplitude modulated wave
plot(t,am_ps);
ylabel('AM Without carrier');

subplot(9,1,5); %plotting the amplitude modulated wave
plot(t,am);
ylabel('AM signal');

subplot(9,1,6); %plotting the amplitude modulated wave
plot(t,am_noised);
ylabel('AM signal + Noise');

subplot(9,1,8); %plotting the amplitude modulated wave
plot(t,am_peaks_final);
ylabel('AM Envelope');

subplot(9,1,9); %plotting the amplitude modulated wave
plot(t,final_final);
ylabel('Modulating');

